﻿using System.IO;
using System.Reflection;
using System.Windows;

namespace WastelandPzPatcher;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    private string? _pzPath = null;
    
    public MainWindow()
    {
        InitializeComponent();
        
        PatchChunkLoading.IsEnabled = false;
        PatchZoomLevels.IsEnabled = false;
        PatchRadio.IsEnabled = false;
        UndoChunkLoading.IsEnabled = false;
        UndoZoomLevels.IsEnabled = false;
        UndoRadio.IsEnabled = false;
        
        PatchChunkLoading.Click += PatchChunkLoading_Click;
        PatchZoomLevels.Click += PatchZoomLevels_Click;
        PatchRadio.Click += PatchRadio_Click;
        UndoChunkLoading.Click += UndoChunkLoading_Click;
        UndoZoomLevels.Click += UndoZoomLevels_Click;
        UndoRadio.Click += UndoRadio_Click;
        
        PzPath.Content = "Unknown";
        Status.Content = "Searching for PZ Install";

        _ = DoSearch();
    }

    private void PatchChunkLoading_Click(object sender, RoutedEventArgs e)
    {
        try {
            ReplaceFile(["zombie", "iso", "WorldStreamer.class"], "WastelandPzPatcher.PzFiles.Patched-WorldStreamer.class");
            Status.Content = "Patched: Chuck Loading";
        } catch (Exception)
        {
            Status.Content = "Failed: Chunk Loading";
        }
    }
    
    private void PatchZoomLevels_Click(object sender, RoutedEventArgs e)
    {
        try {
            ReplaceFile(["zombie", "core", "textures", "MultiTextureFBO2.class"], "WastelandPzPatcher.PzFiles.Patched-MultiTextureFBO2.class");
            Status.Content = "Patched: Zoom Levels";
        } catch (Exception)
        {
            Status.Content = "Failed: Zoom Levels";
        }
    }
    
    private void PatchRadio_Click(object sender, RoutedEventArgs e)
    {
        try {
            ReplaceFile(["zombie", "radio", "ZomboidRadio.class"], "WastelandPzPatcher.PzFiles.Patched-ZomboidRadio.class");
            Status.Content = "Patched: Radio";
        } catch (Exception)
        {
            Status.Content = "Failed: Radio";
        }
    }
    
    private void UndoChunkLoading_Click(object sender, RoutedEventArgs e)
    {
        try {
            ReplaceFile(["zombie", "iso", "WorldStreamer.class"], "WastelandPzPatcher.PzFiles.Original-WorldStreamer.class");
            Status.Content = "Unpatched: Chuck Loading";
        } catch (Exception)
        {
            Status.Content = "Failed: Chuck Loading";
        }
    }
    
    private void UndoZoomLevels_Click(object sender, RoutedEventArgs e)
    {
        try {
            ReplaceFile(["zombie", "core", "textures", "MultiTextureFBO2.class"], "WastelandPzPatcher.PzFiles.Original-MultiTextureFBO2.class");
            Status.Content = "Unpatched: Zoom Levels";
        } catch (Exception)
        {
            Status.Content = "Failed: Zoom Levels";
        }
    }
    
    private void UndoRadio_Click(object sender, RoutedEventArgs e)
    {
        try {
            ReplaceFile(["zombie", "radio", "ZomboidRadio.class"], "WastelandPzPatcher.PzFiles.Original-ZomboidRadio.class");
            Status.Content = "Unpatched: Radio";
        } catch (Exception)
        {
            Status.Content = "Failed: Radio";
        }
    }

    private void ReplaceFile(string[] path, string resourceName)
    {
        var fullPath = Path.Combine(_pzPath!, Path.Combine(path));
        // resource is an embedded resource
        using var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
        if (stream is null)
        {
            throw new("Resource not found");
        }
        using var fileStream = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
        stream.CopyTo(fileStream);
    }

    private async Task DoSearch()
    {
        await Task.Run(FindProjectZomboidInstallPath);
        Dispatcher.Invoke(() =>
        {
            if (_pzPath is null)
            {
                PzPath.Content = "Not Found";
                Status.Content = "PZ Install Not Found";
            }
            else
            {
                PatchChunkLoading.IsEnabled = true;
                PatchZoomLevels.IsEnabled = true;
                PatchRadio.IsEnabled = true;
                UndoChunkLoading.IsEnabled = true;
                UndoZoomLevels.IsEnabled = true;
                UndoRadio.IsEnabled = true;
                PzPath.Content = _pzPath;
                Status.Content = "Ready";
            }
        });
    }

    private void FindProjectZomboidInstallPath()
    {
        var drives = DriveInfo.GetDrives();
        var pathsToSearch = new Queue<string>();
        foreach (var drive in drives) pathsToSearch.Enqueue(Path.Combine(drive.RootDirectory.FullName, "Program Files (x86)", "Steam", "steamapps", "common"));
        foreach (var drive in drives.Where(x => x.DriveType != DriveType.Network)) pathsToSearch.Enqueue(drive.RootDirectory.FullName);
        
        while (pathsToSearch.Count != 0)
        {
            var currentPath = pathsToSearch.Dequeue();
            Dispatcher.Invoke(() => PzPath.Content = "Scanning: " + currentPath);
            var pzPath = SearchForProjectZomboid(currentPath);
            if (string.IsNullOrEmpty(pzPath))
            {
                try
                {
                    var subdirectories = Directory.GetDirectories(currentPath);
                    foreach (var subdirectory in subdirectories) pathsToSearch.Enqueue(subdirectory);
                } catch (Exception) { /* Ignore */ }
                continue;
            }
            _pzPath = pzPath;
            return;
        }
    }
    
    private string? SearchForProjectZomboid(string directory)
    {
        try
        {
            // Check if the current directory contains "ProjectZomboid" and "zombie" subdirectory with "ZombieGlobals.class" file
            var projectZomboidDirectory = Path.Combine(directory, "ProjectZomboid");
            var zombieDirectory = Path.Combine(projectZomboidDirectory, "zombie");
            var zombieGlobalsFile = Path.Combine(zombieDirectory, "ZomboidGlobals.class");
            if (Directory.Exists(projectZomboidDirectory) && Directory.Exists(zombieDirectory) && File.Exists(zombieGlobalsFile))
            {
                return projectZomboidDirectory;
            }
        }
        catch (UnauthorizedAccessException)
        {
            // Ignore and continue searching other directories
        }

        return null;
    }
}